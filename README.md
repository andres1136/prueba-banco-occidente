# Prueba Banco de Occidente

Este proyecto fue generado con [Angular CLI](https://github.com/angular/angular-cli) version 11.2.15.

## Librerias usadas

- Bootstrap 4.
- Bootstrap-icons.
- jquery
- @popperjs

## Instalar dependencias

Correr el comando `npm install` para instalar las depencias necesarias para la correcta ejecución del proyecto.

## Iniciar en ambiente local

Correr `ng serve` para crear un servidor de desarrollos. Navegar en la dirección `http://localhost:4200/`. La aplicación se actualizara directamente cuando se realiza un cambio de archivo.
