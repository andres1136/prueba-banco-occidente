import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  private urlApi = 'https://voxhto8os0.execute-api.us-east-2.amazonaws.com/Prueba/prueba';

  public dataApi : any = null;

  constructor(private httpClient: HttpClient) {}
  
  // title = 'prueba-banco-occidente';

  ngOnInit() {
    this.dataApi = '';
    this.getDataApi();
  }

  private getDataApi() {
    const nit = '800220154';
    const url = `${this.urlApi}`;
    this.httpClient
        .post(url, { nit: nit })
        .subscribe(apiData => {
          this.dataApi = apiData
        })
  }

}
