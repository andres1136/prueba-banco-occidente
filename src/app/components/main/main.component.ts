import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  @Input() data:any;

  constructor() { }

  ngOnInit(): void {
    this.data = {
      nit: ''
    };
  }

  get nitSplit () {
    if (this.data.nit !== ''){
      let dataSplit = this.data.nit.match(/.{1,3}/g);
      dataSplit = dataSplit.join(' ');
      return dataSplit;
    }
  }

}
